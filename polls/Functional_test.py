from unittest import TestCase
from selenium import webdriver
from selenium.webdriver.common.by import By
import time


class FunctionalTest(TestCase):

    def setUp(self):
        self.browser = webdriver.Firefox()
        self.browser.implicitly_wait(2)

    def tearDown(self):
        self.browser.quit()

    def test_title(self):
        self.browser.get('http://localhost:8000')
        self.assertIn('Busco Ayuda', self.browser.title)

    def test_hacerLogin(self):
        self.browser.get('http://localhost:8000')
        link = self.browser.find_element_by_id('id_login')
        link.click()
        time.sleep(1)
        user = self.browser.find_element_by_id('user')
        user.send_keys('juan645')
        password = self.browser.find_element_by_id('password')
        password.send_keys('clave123')
        btnIniciar = self.browser.find_element_by_id('id_iniciar_login')
        btnIniciar.click()
        time.sleep(3)
        btnLogOut = self.browser.find_element_by_id('id_login')
        self.assertIn('Logout', btnLogOut.text)

    def test_edit(self):
        self.browser.get('http://localhost:8000')
        link = self.browser.find_element_by_id('id_login')
        link.click()
        time.sleep(1)
        user = self.browser.find_element_by_id('user')
        user.send_keys('juan645')
        password = self.browser.find_element_by_id('password')
        password.send_keys('clave123')
        btnIniciar = self.browser.find_element_by_id('id_iniciar_login')
        btnIniciar.click()
        time.sleep(3)
        btnLogOut = self.browser.find_element_by_id('id_login')
        time.sleep(3)
        link = self.browser.find_element_by_id('id_editar')
        link.click()
        time.sleep(3)
        form = self.browser.find_element_by_id('editar_modal')
        telefono = form.find_element_by_id('id_telefono')
        telefono.clear();
        telefono.send_keys('317302457899')
        correo = form.find_element_by_id('id_correo')
        correo.clear();
        correo.send_keys('jd.patino1@uniandes.edu.com')
        botonGrabar = form.find_element_by_id('id_grabar')
        botonGrabar.click()
        time.sleep(3)
        link = self.browser.find_element_by_id('id_editar')
        link.click()
        time.sleep(3)
        form = self.browser.find_element_by_id('editar_modal')
        telefono = form.find_element_by_id('id_telefono')
        self.assertIn('317302457899', telefono.get_attribute('value'))

    def test_registro(self):
        self.browser.get('http://localhost:8000')
        link = self.browser.find_element_by_id('id_register')
        link.click()
        time.sleep(5)

        nombre = self.browser.find_element_by_id('id_nombre')
        nombre.send_keys('Juan Daniel')

        apellidos = self.browser.find_element_by_id('id_apellidos')
        apellidos.send_keys('Arevalo')

        experiencia = self.browser.find_element_by_id('id_aniosExperiencia')
        experiencia.send_keys('5')

        self.browser.find_element_by_xpath("//select[@id='id_tiposDeServicio']/option[text()='Desarrollador Web']").click()
        telefono = self.browser.find_element_by_id('id_telefono')
        telefono.send_keys('3173024578')

        correo = self.browser.find_element_by_id('id_correo')
        correo.send_keys('jd.patino1@uniandes.edu.co')

        #imagen = self.browser.find_element_by_id('id_imagen')
        #imagen.send_keys('C:\Users\asistente\Desktop\developer.jpg')

        nombreUsuario = self.browser.find_element_by_id('id_username')
        nombreUsuario.send_keys('juan645')

        clave = self.browser.find_element_by_id('id_password')
        clave.send_keys('clave123')

        botonGrabar = self.browser.find_element_by_id('id_grabar')
        botonGrabar.click()
        self.browser.implicitly_wait(3)
        span = self.browser.find_element(By.XPATH, '//span[text()="Juan Daniel Arevalo"]')

        self.assertIn('Juan Daniel Arevalo', span.text)

    def test_verDetalle(self):
        self.browser.get('http://localhost:8000')
        span = self.browser.find_element(By.XPATH, '//span[text()="Juan Daniel Arevalo"]')
        span.click()
        time.sleep(7)
        h2 = self.browser.find_element(By.XPATH, '//h2[text()="Juan Daniel Arevalo"]')
        self.assertIn('Juan Daniel Arevalo', h2.text)

    def test_comment(self):
        self.browser.get('http://localhost:8000')
        span = self.browser.find_element(By.XPATH, '//span[text()="camilo forero"]')
        span.click()
        time.sleep(3)
        h2 = self.browser.find_element(By.XPATH, '//h2[text()="camilo forero"]')
        self.assertIn('camilo forero', h2.text)
        time.sleep(1)
        correo = self.browser.find_element_by_id('correo')
        correo.send_keys('camus35@hotmail.com')
        comentario = self.browser.find_element_by_id('comentario')
        comentario.send_keys('Comment test')
        time.sleep(1)
        botonGrabar = self.browser.find_element_by_id('id_guarda_comentario')
        botonGrabar.click()

    def test_searchTrabajadores(self):
        self.browser.get('http://localhost:8000')
        buscar = self.browser.find_element_by_id('buscar')
        buscar.send_keys('Desarrollador Web')
        time.sleep(1)
        trabajadores = self.browser.find_elements_by_class_name('panel-heading')

        for trabajador in trabajadores:
            self.assertIn('Desarrollador Web', trabajador.text)

    def test_independientesRegistrados(self):
        self.browser.get('http://localhost:8000')
        time.sleep(1)

        span1 = self.browser.find_element(By.XPATH, '//span[text()="camilo forero"]')
        span2 = self.browser.find_element(By.XPATH, '//span[text()="Juan Daniel Arevalo"]')
        span3 = self.browser.find_element(By.XPATH, '//span[text()="raul  gonzalez"]')

        self.assertIn('camilo forero', span1.text)
        self.assertIn('Juan Daniel Arevalo', span2.text)
        self.assertIn('raul gonzalez', span3.text)

        trabajadores = self.browser.find_elements_by_class_name('panel-heading')
        self.assertIn('Desarrollador Web', trabajadores[0].text)
        self.assertIn('Desarrollador Web', trabajadores[1].text)
        self.assertIn('python', trabajadores[2].text)
